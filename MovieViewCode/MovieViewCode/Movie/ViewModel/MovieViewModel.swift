import UIKit

struct MovieViewModel: Decodable {
    let isLoading: Bool = false
    let movieList: [Movie]
}
