import UIKit

struct ConfigUrl {
    static let baseUrl = "https://api.themoviedb.org/3/"
    static let apiKey = "api_key=c3ba56c1294229258cc0e1e092d0706c"
    static let language = "&language=pt-BR"
    static let popular = "movie/popular?"
    static let page = "&page=1"
    static let imageBaseURL = "https://image.tmdb.org/t/p/w500"
    static let brasil = "&language=pt-BR&page=1&region=BR"
}

