import UIKit

final class MovieRepository: MovieRepositoryType {

    private let http: APIClient
    
    init(http: APIClient) {
        self.http = http
    }

    func fetchMovies(completion: @escaping (Result<MovieList, Error>) -> Void) {
        http.request(MovieService.popular) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                do {
                    let object = try JSONDecoder().decode(MovieList.self, from: data)
                    completion(.success(object))
                } catch {
                    completion(.failure(error))
                }
            }
        }
    }
}
