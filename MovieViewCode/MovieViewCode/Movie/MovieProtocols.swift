import UIKit

protocol MoviePresenterType: AnyObject {
    func fetchMovieList()
}

protocol MovieViewControllerType: AnyObject {
    func show(viewModel: MovieViewModel)
}

protocol MovieViewControllerDelegate: AnyObject {
    func showMovieDetails(movie: Movie)
}
