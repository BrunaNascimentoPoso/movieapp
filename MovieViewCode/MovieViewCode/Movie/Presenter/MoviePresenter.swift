import UIKit

class MoviePresenter: MoviePresenterType {
    
    private let repository: MovieRepositoryType
    
    weak var controller: MovieViewControllerType?
    
    init(repository: MovieRepositoryType){
        self.repository = repository
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func fetchMovieList() {
        repository.fetchMovies() { (result) in
            switch result {
            case .success(let items):
                let model = MovieViewModel(movieList: items.results)
                self.controller?.show(viewModel: model)
            case .failure(let error):
                print("\(self) error on get movies: \(error)")
            }
        }
    }
    
//    func showAlertFailure() {
//        let presenter = MoviePresenter(repository: repository)
//        let contentView = MovieView()
//        let movieViewController = MovieViewController(presenter: presenter, contentView: contentView)
//        movieViewController.showAlertFailure()
//        let alert = UIAlertController(title: "Alert", message: "Error on get popular movies", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
//        present.presentedViewController(alert, animated: true, completion: nil)
//    }
}
