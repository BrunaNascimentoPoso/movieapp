import UIKit

final class MovieDetailViewController: UIViewController {

    private let movieDetailView: MovieDetailView
    private let movie: Movie
    
    init (movie: Movie, contentView: MovieDetailView) {
        self.movie = movie
        self.movieDetailView = contentView
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = movieDetailView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        movieDetailView.show(viewModel: movie)
    }
}
