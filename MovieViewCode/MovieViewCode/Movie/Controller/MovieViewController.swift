import UIKit

final class MovieViewController: UIViewController {
    
    private let contentView: MovieView
    private let presenter: MoviePresenterType
    
    weak var delegate: MovieViewControllerDelegate?
    
    init(presenter: MoviePresenterType, contentView: MovieView){
        self.presenter = presenter
        self.contentView = contentView
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationItem()
        presenter.fetchMovieList()
        bindLayoutEvents()
    }
    
    override func loadView() {
        view = contentView
    }
    
    private func bindLayoutEvents() {
        contentView.didSelect = { [weak self] movie in
            self?.delegate?.showMovieDetails(movie: movie)
        }
    }

    private func setupNavigationItem() {
        self.navigationItem.title = MovieStrings.popularList
    }
    
//    func showAlertFailure() {
//        let alert = UIAlertController(title: "Alert", message: "Error on get popular movies", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
//        present(alert, animated: true, completion: nil)
//    }
}

extension MovieViewController: MovieViewControllerType {
    
    func show(viewModel: MovieViewModel) {
        contentView.show(movies: viewModel.movieList)
    }
}
