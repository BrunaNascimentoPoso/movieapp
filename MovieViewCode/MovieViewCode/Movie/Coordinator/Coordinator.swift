import UIKit

public protocol Coordinator {
    var currentViewController: UIViewController? { get }
    var navigationController: UINavigationController { get }
    func start(with navigationType: NavigationType)
}

public enum NavigationType: Equatable {
    case push
    case root
    case present
}

public extension Coordinator {
    
    func show(_ viewController: UIViewController, with navigationType: NavigationType, animated: Bool = true) {
        switch navigationType {
        case .push:
            navigationController.pushViewController(viewController, animated: animated)
        case .root: navigationController.setViewControllers([viewController], animated: true)
        case .present:
            if !(viewController.modalPresentationStyle == .custom || viewController.modalPresentationStyle == .overFullScreen) {
                viewController.modalPresentationStyle = .fullScreen
            }
            navigationController.present(viewController, animated: animated, completion: nil)
        }
    }
}
