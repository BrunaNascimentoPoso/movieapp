import UIKit

final class MovieCoordinator: Coordinator {
    
    public var currentViewController: UIViewController?
    public var navigationController: UINavigationController
    
    private let network: MovieRepository
    
    init(navigationController: UINavigationController, network: MovieRepository) {
        self.navigationController = navigationController
        self.network = network
    }
    
    public func start(with navigationType: NavigationType = .push) {
        createListViewController()
    }
    
    private func createListViewController() {
        let presenter = MoviePresenter(repository: network)
        let contentView = MovieView()
        let controller = MovieViewController(presenter: presenter, contentView: contentView)
        presenter.controller = controller
        controller.delegate = self
        currentViewController = controller
        show(controller, with: .push)
    }
    
    private func createDetailViewController(movie: Movie) {
        let movieDetailView = MovieDetailView()
        let controller = MovieDetailViewController(movie: movie, contentView: movieDetailView)
        currentViewController = controller
        show(controller, with: .push)
    }
}

extension MovieCoordinator: MovieViewControllerDelegate {
    
    func showMovieDetails(movie: Movie) {
        createDetailViewController(movie: movie)
    }
}
