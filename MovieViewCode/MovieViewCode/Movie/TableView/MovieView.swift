import UIKit

final class MovieView: UIView {

    private let dataSource = MovieTableViewDataSource(movieList: [])
    
    var didSelect: ((Movie) -> Void)?
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    init() {
        super.init(frame: .zero)
        backgroundColor = .white
        buildViewHierarchy()
        addConstraints()
        setupTableView()
        bindLayoutEvents()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show(movies: [Movie]) {
        dataSource.movieList = movies
        dataSource.register(in: self.tableView)
        tableView.reloadData()
    }
    
    private func buildViewHierarchy() {
        addSubview(tableView)
    }
    
    private func bindLayoutEvents() {
        dataSource.didSelect = { [weak self] movie in
            self?.didSelect?(movie)
        }
    }
    
    private func setupTableView() {
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        dataSource.register(in: self.tableView)
    }
    
    private func addConstraints() {
        tableView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
}
