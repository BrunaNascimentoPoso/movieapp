@testable import MovieViewCode

final class MoviePresenterSpy: MoviePresenterType {
    
    private(set) var hasCalledfetchMovieList = false
    
    func fetchMovieList() {
        hasCalledfetchMovieList = true
    }

}
