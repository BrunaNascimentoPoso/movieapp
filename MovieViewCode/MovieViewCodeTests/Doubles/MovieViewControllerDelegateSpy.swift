@testable import MovieViewCode

final class MovieViewControllerDelegateSpy: MovieViewControllerDelegate {
    
    private(set) var didCallShowMovieDetails = false
    
    func showMovieDetails(movie: Movie) {
        didCallShowMovieDetails = true
    }
}
