import Foundation
@testable import MovieViewCode

final class APICLientMock: APIClient {
    
    private(set) public var requestWasCalled = false
    
    override func request(_ resource: MovieService, completion: @escaping ((Result<Data, Error>) -> Void)) {
        requestWasCalled = true
    }
    
}
