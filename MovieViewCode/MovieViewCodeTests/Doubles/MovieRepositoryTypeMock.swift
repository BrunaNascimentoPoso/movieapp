@testable import MovieViewCode

final class MovieRepositoryTypeMock: MovieRepositoryType {
    
    var error: MovieServiceError?
    
    private(set) var fetchMoviesWasCalled = false
    
    let movie = Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")
    
    var fetchMoviesResult: Result<MovieList, Error> = {
//       return .success(MovieList(results: [Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")]))

        var error: Error!
        return .failure(error)
    }()

    func fetchMovies(completion: @escaping (Result<MovieList, Error>) -> Void) {
        fetchMoviesWasCalled = true
        completion(fetchMoviesResult)
//        if let error = error {
//            completion(.failure(error))
//        }
//        completion(
//            .success(MovieList(results: [movie]))
//        )
    }
}


