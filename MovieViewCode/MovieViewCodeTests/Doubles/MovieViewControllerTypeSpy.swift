@testable import MovieViewCode

final class MovieViewControllerTypeSpy: MovieViewControllerType {
    
    private(set) var didCallShowViewModel = false
    
    func show(viewModel: MovieViewModel) {
        didCallShowViewModel = true
        
    }
    
}
