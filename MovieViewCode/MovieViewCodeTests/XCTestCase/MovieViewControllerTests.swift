import XCTest
@testable import MovieViewCode

final class MovieViewControllerTests: XCTestCase {

    var sut: MovieViewController!
    var presenterSpy: MoviePresenterSpy!
    var delegateSpy: MovieViewControllerDelegateSpy!
    var contentView: MovieView!
    
    let movie = Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")
    
    override func setUp() {
        super.setUp()
        presenterSpy = MoviePresenterSpy()
        delegateSpy = MovieViewControllerDelegateSpy()
        contentView = MovieView()
        sut = MovieViewController(presenter: presenterSpy, contentView: contentView)
        sut.delegate = delegateSpy
        
    }

    func testLoadView() {
        sut.loadView()
        XCTAssertEqual(sut.view, contentView)
    }
    
    func testPresenterFetchMovieList() {
        presenterSpy.fetchMovieList()
        XCTAssertTrue(presenterSpy.hasCalledfetchMovieList, "should call presenter fetch movies")
    }
    
    func testShowViewModel() {
        let movieList = MovieList(results: [movie])
        let viewModel = MovieViewModel(movieList: movieList.results)
        sut.show(viewModel: viewModel)
        XCTAssertEqual(sut.view, contentView)
    }

}
