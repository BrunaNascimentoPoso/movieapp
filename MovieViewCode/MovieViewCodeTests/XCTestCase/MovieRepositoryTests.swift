import XCTest
@testable import MovieViewCode

final class MovieRepositoryTests: XCTestCase {

    var sut: MovieRepository!
    var networkMock: APICLientMock!

    override func setUp() {
        networkMock = APICLientMock()
        sut = MovieRepository(http: networkMock)
    }

    func testResquestMovies(){
        sut.fetchMovies() { _ in }
        XCTAssertTrue(networkMock.requestWasCalled)
    }

}
