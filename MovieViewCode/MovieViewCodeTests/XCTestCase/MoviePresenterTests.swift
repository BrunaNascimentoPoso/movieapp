import XCTest
@testable import MovieViewCode

final class MoviePresenterTests: XCTestCase {
    
    var sut: MoviePresenter!
    var repository: MovieRepository!
    var repositoryMock: MovieRepositoryTypeMock!
    var controllerSpy: MovieViewControllerTypeSpy!
    
    let movie = Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")
    
    override func setUp() {
        super.setUp()
        controllerSpy = MovieViewControllerTypeSpy()
        repository = MovieRepository(http: APIClient())
        repositoryMock = MovieRepositoryTypeMock()
        sut = MoviePresenter(repository: repository)
        
        sut.controller = controllerSpy
        
    }
    
//    func testCallControllerShow() {
//        let movieList = MovieList(results: [movie])
//        let viewModel = MovieViewModel(movieList: movieList.results)
//        controllerSpy.show(viewModel: viewModel)
//        
//        XCTAssert(controllerSpy.didCallShowViewModel == true, "should call controller show(viewModel)")
//    }
    
    func testFetchMovieList() {
        let movieList = MovieList(results: [movie])
        let viewModel = MovieViewModel(movieList: movieList.results)
        
        repositoryMock.fetchMovies { (result) in
            switch result {
            case .success:
                self.controllerSpy.show(viewModel: viewModel)
                XCTAssertTrue(self.controllerSpy.didCallShowViewModel)
                
            case .failure(let error):
                XCTFail("Error on get movies: \(error)")
                XCTAssertFalse(self.controllerSpy.didCallShowViewModel)
            }
        }
    }
    
//    func testFetchMovieListSuccess() {
//        let movieList = MovieList(results: [movie])
//        let viewModel = MovieViewModel(movieList: movieList.results)
//    }

}
