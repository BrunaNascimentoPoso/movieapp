import XCTest
@testable import MovieViewCode

final class MovieTableViewDataSourceTests: XCTestCase {

    var sut: MovieTableViewDataSource!
    var tableView: UITableView!
    
    let cell = Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")

    override func setUp() {
        super.setUp()
        let viewModel = MovieViewModel(movieList: [cell])
        sut = MovieTableViewDataSource(movieList: viewModel.movieList)
        tableView = UITableView()
    }

    override func tearDown() {
        super.tearDown()
        sut = nil
    }
    
    func testValueInDataSource() {
        
        tableView.dataSource = sut
        tableView.reloadData()
        
        XCTAssertEqual(sut.tableView(tableView, numberOfRowsInSection: 1), 1, "Has the right number of cells")
    }
    
    func testCellForRowDataSource() {
        reloadData()
        let indexPath = IndexPath(row: 0, section: 1)
        let cell = sut.tableView(tableView, cellForRowAt: indexPath) as? MovieTableViewCell
//        let movieTableViewCell = MovieTableViewCell()
//        XCTAssertEqual(cell, movieTableViewCell)
        XCTAssertNotNil(cell)
    }
    
    func reloadData() {
        tableView.register(MovieTableViewCell.self, forCellReuseIdentifier: "cellId")
        tableView.reloadData()
    }
    
}
