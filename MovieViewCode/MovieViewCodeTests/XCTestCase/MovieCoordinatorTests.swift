import XCTest
@testable import MovieViewCode

final class MovieCoordinatorTests: XCTestCase {
    
    var sut: MovieCoordinator!
    var navigationController = UINavigationController()
    let network = MovieRepository(http: APIClient())
    var window: UIWindow!
    var delegateSpy: MovieViewControllerDelegateSpy!
    
    let movie = Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")
    
    override func setUp() {
        super.setUp()
        window = UIWindow(frame: UIScreen.main.bounds)
        delegateSpy = MovieViewControllerDelegateSpy()
        sut = MovieCoordinator(navigationController: navigationController, network: network)
    }

    override func tearDown() {
        super.tearDown()
        delegateSpy = nil
        sut = nil
    }

    func testCoordinatorCreateListViewController() {
        
        sut.start()
        
//        XCTAssertEqual(sut.currentViewController, window.rootViewController)
//        XCTAssertEqual(sut.currentViewController, MovieViewController.self as? UIViewController)
        
    }
    
    func testCoordinatorCreateDetailViewController() {
        sut.start(with: .push)

        sut.showMovieDetails(movie: movie)
//        XCTAssert(delegateSpy.didCallShowMovieDetails == true, "should call createDetailViewController")
//        XCTAssertEqual(sut.currentViewController, MovieDetailViewController.self as? UIViewController)
//        XCTAssertTrue(window.rootViewController?.presentedViewController is MovieDetailViewController)
    }

}
