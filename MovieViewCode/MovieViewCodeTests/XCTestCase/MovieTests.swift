import XCTest
@testable import MovieViewCode

final class MovieTests: XCTestCase {
    
    var movie: Movie!

    override func setUp() {
        super.setUp()
        movie = Movie()
    }

    override func tearDown() {
        movie = nil
    }
    
    func testMovieSetGet() {
        let movie = Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")
        
        XCTAssertEqual(movie.poster_path, "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg")
        XCTAssertEqual(movie.title, "Eu Sou Todas as Meninas")
        XCTAssertEqual(movie.overview, "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")
    }

}
