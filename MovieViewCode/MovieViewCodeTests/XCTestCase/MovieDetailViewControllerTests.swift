import XCTest
@testable import MovieViewCode

class MovieDetailViewControllerTests: XCTestCase {
    
    var sut: MovieDetailViewController!
    var contentView: MovieDetailView!
    
    let movie = Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")

    override func setUp() {
        contentView = MovieDetailView()
        sut = MovieDetailViewController(movie: movie, contentView: contentView)
    }

    func testLoadView() {
        sut.loadView()
        XCTAssertEqual(sut.view, contentView)
    }
    
    func testViewDidLoad() {
        contentView.show(viewModel: movie)
        XCTAssertEqual(sut.view, contentView)
    }

}
