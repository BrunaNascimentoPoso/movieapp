import Quick
import Nimble
import Nimble_Snapshots
@testable import MovieViewCode

final class MovieViewControllerTestsQN: QuickSpec {
    
    override func spec() {
        var sut: MovieViewController!
        var presenterSpy: MoviePresenterSpy!
        var delegateSpy: MovieViewControllerDelegateSpy!
        var contentView: MovieView!
        
        let movie = Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")
        
        beforeEach {
            presenterSpy = MoviePresenterSpy()
            delegateSpy = MovieViewControllerDelegateSpy()
            contentView = MovieView()
            sut = MovieViewController(presenter: presenterSpy, contentView: contentView)
            sut.delegate = delegateSpy
        }
        
        describe("#loadView") {
            
            beforeEach {
                sut.loadView()
            }
            
            it("should set view as MovieView") {
                sut.loadView()
                expect(sut.view).to(be(contentView))
            }
        }
        
        describe("#viewDidLoad") {
            
            beforeEach {
                presenterSpy.fetchMovieList()
            }
            
            it("should call presenter fetch movies") {
                expect(presenterSpy.hasCalledfetchMovieList).to(beTrue())
            }
        }
        
        describe("show(viewModel)") {
            it("should have a valid snapshot") {
                let movieList = MovieList(results: [movie])
                let viewModel = MovieViewModel(movieList: movieList.results)
                sut.view.frame = CGRect(x: 0, y: 0, width: 343, height: 600)
            
                sut.show(viewModel: viewModel)
                expect(sut.view).to(haveValidSnapshot())
            }
        }
    }
}
