import Quick
import Nimble
@testable import MovieViewCode

final class MovieRepositoryTestsQN: QuickSpec {
    
    override func spec() {
        
        var sut: MovieRepository!
        var networkMock: APICLientMock!
        
        beforeEach {
            networkMock = APICLientMock()
            sut = MovieRepository(http: networkMock)
        }
        
        describe("#getMovies") {
            it("should request movies"){
                sut.fetchMovies() { _ in }
                expect(networkMock.requestWasCalled).to(equal(true))
            }
            
        }
        
    }
}
