import Quick
import Nimble
import Nimble_Snapshots
@testable import MovieViewCode

final class MovieDetailViewControllerTestsQN: QuickSpec {
    
    override func spec() {
        var sut: MovieDetailViewController!
        var contentView: MovieDetailView!
        
        let movie = Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")
        
        beforeEach {
            contentView = MovieDetailView()
            sut = MovieDetailViewController(movie: movie, contentView: contentView)
        }
        
        describe("#loadView") {
            it("should set view as MovieDetailView") {
                sut.loadView()
                expect(sut.view).to(be(contentView))
            }
        }
        
        describe("show(viewModel)") {
            it("should have a valid snapshot") {
                contentView.show(viewModel: movie)
                sut.view.frame = CGRect(x: 0, y: 0, width: 343, height: 600)
                expect(sut.view).to(haveValidSnapshot())
            }
        }
    }
}
