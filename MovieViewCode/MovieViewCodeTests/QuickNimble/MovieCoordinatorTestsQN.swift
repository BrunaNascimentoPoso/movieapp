import Quick
import Nimble
@testable import MovieViewCode

final class MovieCoordinatorTestsQN: QuickSpec {
    
    override func spec() {
        
        var sut: MovieCoordinator!
        let navigationController = UINavigationController()
        let network = MovieRepository(http: APIClient())
        
        let movie = Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")
        
        beforeEach {
            sut = MovieCoordinator(navigationController: navigationController, network: network)
        }
        
        describe("#start") {
            
            context("when flow is assign") {
                
                beforeEach {
                    sut.start()
                }
                
                it("instantiates MovieViewController as the current controller") { expect(sut.currentViewController).to(beAKindOf(MovieViewController.self))
                }
            }
            
            context("when flow is detail") {
                
                beforeEach {
                    sut.start()
                    sut.showMovieDetails(movie: movie)
                }
                
                it("instantiates MovieDetailViewController as the current coordinator") {
                    expect(sut.currentViewController).to(beAKindOf(MovieDetailViewController.self))
                }
            }
        }
        
        describe("#showMovieDetails") {
            
            beforeEach {
                sut.showMovieDetails(movie: movie)
            }
            
            it("should set current coordinator to MovieDetailViewController") {
                expect(sut.currentViewController).to(beAKindOf(MovieDetailViewController.self))
            }
        }
    }
}
