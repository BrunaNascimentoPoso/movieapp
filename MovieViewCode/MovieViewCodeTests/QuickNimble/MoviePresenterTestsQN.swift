import Quick
import Nimble
@testable import MovieViewCode

final class MoviePresenterQN: QuickSpec {
    
    override func spec() {
        
        var sut: MoviePresenter!
        var repositoryMock: MovieRepositoryTypeMock!
        var controllerSpy: MovieViewControllerTypeSpy!
        
        beforeEach {
            controllerSpy = MovieViewControllerTypeSpy()
            repositoryMock = MovieRepositoryTypeMock()
            sut = MoviePresenter(repository: repositoryMock)
            
            sut.controller = controllerSpy
        }
        
        describe("#confirmFetchMovies") {
            
            context("#when receives a failure response") {
                
                beforeEach {
//                    repositoryMock.fetchMoviesResult = .failure()
//                    repositoryMock.error = MovieServiceError.generalError
                    sut.fetchMovieList()
                
                }
                
                it("should call controller hasCalledfetchMovieList with false") {
                    expect(repositoryMock.fetchMoviesWasCalled).to(beTrue())
                    expect(controllerSpy.didCallShowViewModel).to(beFalse())
                }
            }
            
            context("#when receives a success response") {
                
                beforeEach {
                    repositoryMock.fetchMoviesResult = .success(MovieList(results: [Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilhba de tráfico de crianças.")]))
                    sut.fetchMovieList()
                }
                
                it("should call controller hasCalledfetchMovieList with true") {
                    expect(repositoryMock.fetchMoviesWasCalled).to(beTrue())
                    expect(controllerSpy.didCallShowViewModel).to(beTrue())
                }
            }
        }
    }
}
