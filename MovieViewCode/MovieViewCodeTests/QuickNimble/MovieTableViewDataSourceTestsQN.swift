import Quick
import Nimble
@testable import MovieViewCode

final class MovieTableViewDataSourceTestsQN: QuickSpec {
    
    override func spec() {
        var sut: MovieTableViewDataSource!
        var tableView: UITableView!
        let cell = Movie(poster_path: "/gWBXydaQ9vX3OIf0f1wNQ9Z0ElQ.jpg", title: "Eu Sou Todas as Meninas", overview: "Uma detetive implacável se une a uma assassina que ataca os integrantes de uma quadrilha de tráfico de crianças.")
        
        beforeEach {
            let viewModel = MovieViewModel(movieList: [cell])
            sut = MovieTableViewDataSource(movieList: viewModel.movieList)
            tableView = UITableView()
            reloadData()
        }
        
        describe("#numberOfRowsInSection") {
            it("has the right number of cells") {
                expect(sut.tableView(tableView, numberOfRowsInSection: 1)).to(equal(1))
            }
        }
        
        describe("#cellForRowAtIndexPath") {
            it("has the MovieTableViewCell cell type") {
                let indexPath = IndexPath(row: 0, section: 1)
                let cell = sut.tableView(tableView, cellForRowAt: indexPath) as? MovieTableViewCell
                expect(cell).to(beAKindOf(MovieTableViewCell.self))
            }
        }
        
        func reloadData() {
            tableView.register(MovieTableViewCell.self, forCellReuseIdentifier: "cellId")
            tableView.reloadData()
        }
    }
}
